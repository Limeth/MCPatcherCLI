package cz.limeth.mcpatchercli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.prupe.mcpatcher.Hook;
import lombok.extern.java.Log;

@Log
public class Main
{
    public static void main(String[] args) throws Exception
    {
        Arguments arguments = new Arguments();
        JCommander jCommander = new JCommander(arguments);

        try
        {
            jCommander.parse(args);

            if(arguments.help || args.length <= 0)
            {
                jCommander.usage();
                return;
            }
        }
        catch(ParameterException e)
        {
            System.out.println(e.getLocalizedMessage());
            return;
        }
        finally
        {
            if(!mcpatcherAvailable())
            {
                System.out.println("MCPatcher is not available. Make sure it is called " +
                        "'mcpatcher.jar' and located in the same directory as MCPatcherCLI.");
                //noinspection ReturnInsideFinallyBlock
                return;
            }
        }

        Hook.run(arguments);
    }

    public static boolean mcpatcherAvailable()
    {
        try
        {
            Class.forName("com.prupe.mcpatcher.MCPatcher");
            return true;
        }
        catch (ClassNotFoundException e)
        {
            return false;
        }
    }
}
