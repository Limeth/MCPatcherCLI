package com.prupe.mcpatcher;

import com.beust.jcommander.internal.Lists;
import com.prupe.mcpatcher.launcher.version.Version;
import com.prupe.mcpatcher.launcher.version.VersionList;
import cz.limeth.mcpatchercli.Arguments;
import cz.limeth.mcpatchercli.reflection.ReflectedField;
import cz.limeth.mcpatchercli.reflection.ReflectionUtil;
import cz.limeth.mcpatchercli.reflection.WrappedReflection;
import lombok.SneakyThrows;

import java.io.*;
import java.util.*;

import static cz.limeth.mcpatchercli.reflection.ReflectionUtil.*;

public class Hook
{
    public static void run(Arguments args)
    {
        setLogLevel(args.logLevel);

        WrappedReflection<MCPatcher> mcPatcher = wrap(MCPatcher.class);
        ReflectedField<UserInterface> ui = mcPatcher.field("ui");
        ReflectedField<ProfileManager> profileManager = mcPatcher.field("profileManager");
        byte exitStatus = 1;
        File minecraftDir = args.minecraftDirectory != null ? args.minecraftDirectory
                                                            : ReflectionUtil.call(MCPatcherUtils.class, "getDefaultGameDir");
        ui.set(new AutoCLI(args));

        if(!ui.get().locateMinecraftDir(minecraftDir.getAbsolutePath())) {
            System.exit(exitStatus);
        }

        Config config = Config.getInstance();
        profileManager.set(new ProfileManager(config));
        profileManager.get().setRemote(false /*guiEnabled && config.fetchRemoteVersionList*/);

        ui.get().show();
        Util.logOSInfo();

        if(!MCPatcher.VERSION_STRING.equals(config.patcherVersion)) {
            config.patcherVersion = MCPatcher.VERSION_STRING;
            config.betaWarningShown = false;
        }

        if(ui.get().go(profileManager.get())) {
            exitStatus = 0;
        }

        if(ui.get().shouldExit()) {
            MCPatcher.saveProperties();
            System.exit(exitStatus);
        }
    }

    public static void patch() throws PatchException
    {
        WrappedReflection<MCPatcher> mcPatcher = ReflectionUtil.wrap(MCPatcher.class);
        WrappedReflection<MinecraftJar> minecraft = mcPatcher.<MinecraftJar>field("minecraft").wrap();

        MCPatcher.modList.refreshInternalMods();
        MCPatcher.modList.setApplied(true);

        mcPatcher.field("modifiedClasses").wrap().call("clear");
        mcPatcher.field("addedClasses").wrap().call("clear");

        try {
            Logger.log(0);
            Logger.log(0, "Patching...");
            mcPatcher.<UserInterface>field("ui").get().setStatusText("Patching %s...", minecraft.getInstance().getOutputFile().getName());

            for(Mod mod : MCPatcher.modList.getAll())
            {
                mod.resetCounts();
                Logger.log(0, "[%s] %s %s - %s", mod.isEnabled() && mod.okToApply() ? "*" : " ", mod.getName(), mod.getVersion(), mod.getDescription());
            }

            if(!MCPatcher.modList.getSelected().isEmpty())
                Logger.log(0);

            if(!MCPatcher.profileManager.getOutputJar().getParentFile().isDirectory()
                && !MCPatcher.profileManager.getOutputJar().getParentFile().mkdirs())
                throw new IOException("Could not create output parent directories.");

            mcPatcher.call("applyMods");
            mcPatcher.call("writeProperties");
            minecraft.getInstance().checkOutput();
            minecraft.getInstance().closeStreams();
            minecraft.getInstance().clearClassFileCache();
//            MCPatcher.profileManager.createOutputProfile(MCPatcher.modList.getOverrideVersionJson(),
//                    MCPatcher.modList.getExtraJavaArguments(), MCPatcher.modList.getExtraLibraries());
            Logger.log(0);
            Logger.log(0, "Done!");
        } catch (Throwable t) {
            Logger.log(0);
            Logger.log(0, "Restoring original profile due to previous error");
            MCPatcher.unpatch();
            throw new PatchException(t);
        }
    }

    public static void setLogLevel(int level)
    {
        set(Logger.class, "logLevel", level);
    }

    public static int getLogLevel()
    {
        return get(Logger.class, "logLevel");
    }

    public static void refresh(String version, ProfileManager unwrappedPM, UserInterface ui, File versionJar, File versionJson, String versionId)
    {
        WrappedReflection<ProfileManager> pm = wrap(unwrappedPM);
        ReflectedField<Boolean> ready = pm.field("ready");
        ready.set(false);
        pm.call("rebuildRemoteVersionList", Lists.newArrayList(UserInterface.class, Boolean.TYPE),
                Lists.newArrayList(ui, false));
        ProfileManager$rebuildLocalVersionList(unwrappedPM, ui, versionJar, versionJson, versionId);

        pm.set("inputVersion", version);
        pm.set("outputVersion", version);

        ready.set(true);
    }

    @SneakyThrows
    private static void ProfileManager$rebuildLocalVersionList(ProfileManager unwrappedPM, UserInterface ui, File versionJar, File versionJson, String versionId)
    {
        WrappedReflection<ProfileManager> pm = wrap(unwrappedPM);
        List<String> unmoddedVersions = pm.get("unmoddedVersions");
        List<String> releaseVersions = pm.get("releaseVersions");
        VersionList remoteVersions = pm.get("remoteVersions");
        WrappedReflection<?> originalVersion = wrap(classOf("com.prupe.mcpatcher.ProfileManager$OriginalVersion"));

        unmoddedVersions.clear();
        releaseVersions.clear();
        originalVersion.call("clear");

        for(Version remote : remoteVersions.getVersions())
        {
            if(!remote.getId().equals(versionId))
                continue;

            if(MinecraftVersion.parseVersion(remote.getId()) == null)
                continue;

            Version local = VersionBridge.getSpecific(versionJar, versionJson, versionId);

            if(local != null && versionJar.isFile() && versionJson.isFile())
            {
                unmoddedVersions.add(0, local.getId());

                if(!local.isSnapshot())
                    releaseVersions.add(0, local.getId());

                //originalVersion.call("register", Lists.newArrayList(Version.class), Lists.newArrayList(local));
                ui.setStatusText("Found %d installed versions...", unmoddedVersions.size());
            }
        }

        if(unmoddedVersions.isEmpty())
            throw new IOException("No installed unmodded versions found");
    }

    public static void listMods()
    {
        for(Mod mod : MCPatcher.modList.getAll())
        {
            int index = MCPatcher.modList.indexOf(mod);
            String name = mod.getName();
            String version = mod.getVersion();
            String author = mod.getAuthor();
            String description = mod.getDescription();
            String website = mod.getWebsite();

            System.out.println("[" + index + "] " + name);
            System.out.println("\tVersion: " + version);
            System.out.println("\tAuthor: " + author);
            System.out.println("\tDescription: " + description);
            System.out.println("\tWebsite: " + website);
        }
    }

    public static Optional<MinecraftVersion> getVersion(File file)
    {
        if(!file.isFile())
            return Optional.empty();

        String md5 = wrap(Util.class).<String>method("computeMD5", File.class).call(file);
        MinecraftVersion version = wrap(MinecraftVersion.class).<Map<String, MinecraftVersion>>field("alternateMD5s").get().get(md5);

        return Optional.ofNullable(version);
    }

    public static class PatchException extends Exception
    {
        public PatchException(Throwable cause)
        {
            super(cause);
        }
    }
}
