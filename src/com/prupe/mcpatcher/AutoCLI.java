package com.prupe.mcpatcher;

import cz.limeth.mcpatchercli.Arguments;
import cz.limeth.mcpatchercli.reflection.ReflectionUtil;
import cz.limeth.mcpatchercli.reflection.WrappedReflection;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.java.Log;

import java.io.File;
import java.util.List;

@Log
@Value
@EqualsAndHashCode(callSuper = false)
public class AutoCLI extends UserInterface.CLI
{
    private final Arguments args;

    @Override
    boolean locateMinecraftDir(String enteredMCDir)
    {
        File file = new File(enteredMCDir).getAbsoluteFile();

        if(!file.isDirectory())
            throw new IllegalArgumentException("Specified path is not a minecraft directory.");

        WrappedReflection<MCPatcherUtils> utils = ReflectionUtil.wrap(MCPatcherUtils.class);
        utils.set("minecraftDir", file);
        utils.set("gameDir", file);

        log.info("--- IGNORE THE FOLLOWING ERROR ---");

        boolean result = Config.load(file, false);

        log.info("--- IGNORE THE PRECEDING ERROR ---");

        //noinspection ConstantConditions
        return result;
    }

    @Override
    File chooseMinecraftDir(File enteredMCDir)
    {
        return enteredMCDir;
    }

    @SneakyThrows
    @Override
    boolean go(ProfileManager profileManager)
    {
        MinecraftVersion version = selectVersion();

        Hook.refresh(version.getVersionString(), profileManager, this, args.inputJar, args.inputJson, version.getVersionString());

        System.out.println("--- IGNORE THE FOLLOWING ERROR ---");
        MCPatcher.refreshMinecraftPath();
        System.out.println("--- IGNORE THE PRECEDING ERROR ---");

        MCPatcher.refreshModList();

        if(args.listMods)
        {
            System.out.println();
            System.out.println("Available mods for selected version:");
            System.out.println();
            Hook.listMods();
            return false;
        }

        if(args.selectedMods != null)
        {
            try
            {
                selectMods(args.selectedMods);
            }
            catch(InvalidModException e)
            {
                System.out.println("Unknown mod '" + e.getModName() + "'.");
                return false;
            }
        }

        WrappedReflection<MinecraftJar> jar = ReflectionUtil.wrap(MCPatcher.class).<MinecraftJar>field("minecraft").wrap();
        jar.field("inputFile").set(args.inputJar);
        jar.field("outputFile").set(args.outputJar);

        MCPatcher.checkModApplicability();

        if(Hook.getLogLevel() >= Logger.LOG_MOD)
        {
            System.out.println();
            System.out.println("#### Class map:");
            MCPatcher.showClassMaps(System.out, false);
        }

        Hook.patch();

        if(Hook.getLogLevel() >= Logger.LOG_MOD)
        {
            System.out.println();
            System.out.println("#### Patch summary:");
            MCPatcher.showPatchResults(System.out);
        }

        return true;
    }

    public MinecraftVersion selectVersion() throws InvalidVersionException
    {
        if(args.version != null)
        {
            MinecraftVersion version = MinecraftVersion.parseVersion(args.version);

            if(version != null)
                return version;

            throw new InvalidVersionException("Unknown Minecraft version '" + args.version + "'.");
        }
        else
        {
            return Hook.getVersion(args.inputJar).orElseThrow(() -> new InvalidVersionException(
                    "Unknown Minecraft version of JAR '" + args.inputJar.getAbsolutePath()
                    + "'. Is it modded? Try manually specifying the Minecraft version using the '--version' argument."
            ));
        }
    }

    public static void selectMods(List<String> selectedMods) throws InvalidModException
    {
        MCPatcher.modList.disableAll();

        if(selectedMods.size() == 1 && selectedMods.get(0).length() == 0)
            return;

        for(String modName : selectedMods)
        {
            Mod mod;

            try
            {
                mod = MCPatcher.modList.get(Integer.parseInt(modName));
            }
            catch(NumberFormatException e)
            {
                mod = MCPatcher.modList.get(modName);
            }

            if(mod == null)
                throw new InvalidModException(modName);

            MCPatcher.modList.selectMod(mod, true);
        }
    }

    @EqualsAndHashCode(callSuper = false)
    @Value
    public static class InvalidVersionException extends Exception
    {
        public InvalidVersionException(String desc)
        {
            super(desc);
        }
    }

    @EqualsAndHashCode(callSuper = false)
    @Value
    public static class InvalidModException extends Exception
    {
        private final String modName;
    }
}
