package cz.limeth.mcpatchercli.reflection;

import lombok.Getter;
import lombok.NonNull;

import java.util.List;

@SuppressWarnings({"unused", "TypeParameterHidesVisibleType"})
public final class WrappedReflection<T>
{
    private final Class<? super T> cls;
    @Getter private final T instance;

    /**
     * @param cls The class with wanted properties
     * @param instance The instance to get the properties from, or {@code null} for static access
     */
    public WrappedReflection(@NonNull Class<? super T> cls, T instance)
    {
        if(instance != null)
        {
            Class<?> instanceClass = instance.getClass();

            if (!cls.isAssignableFrom(instanceClass))
                throw new IllegalArgumentException("Class '" + cls + "' is not extended or implemented by '" + instanceClass + "'.");
        }

        this.cls = cls;
        this.instance = instance;
    }

    /**
     * @param instance The instance to get the properties from
     */
    @SuppressWarnings("unchecked")
    public WrappedReflection(@NonNull T instance)
    {
        this.cls = (Class<? super T>) instance.getClass();
        this.instance = instance;
    }

    /**
     * @param cls The class to access statically
     */
    public WrappedReflection(@NonNull Class<T> cls)
    {
        this(cls, null);
    }

    public WrappedReflection<T> with(Class<? super T> cls)
    {
        return new WrappedReflection<>(cls, instance);
    }

    @SuppressWarnings("unchecked")
    public WrappedReflection<T> withInstanceClass()
    {
        if(instance == null)
            throw new NullPointerException("This WrappedException is a static pointer -- instance is 'null'.");

        return with((Class<? super T>) instance.getClass());
    }

    public <T> T get(String fieldName)
    {
        return ReflectionUtil.get(cls, instance, fieldName);
    }

    public void set(String fieldName, Object value)
    {
        ReflectionUtil.set(cls, instance, fieldName, value);
    }

    public <T> T call(String methodName, List<Class<?>> parameterTypes, List<Object> parameters)
    {
        return ReflectionUtil.call(cls, instance, methodName, parameterTypes, parameters);
    }

    public <T> T call(String methodName)
    {
        return ReflectionUtil.call(cls, instance, methodName);
    }

    @SuppressWarnings("unchecked")
    public T make(List<Class<?>> parameterTypes, List<Object> parameters) throws ClassCastException
    {
        try
        {
            Class<T> castClass = (Class<T>) cls;

            return ReflectionUtil.make(castClass, parameterTypes, parameters);
        }
        catch(ClassCastException e)
        {
            throw new ClassCastException("Cannot instantiate the class, since the property class is a parent class of the instance. Consider using the 'withInstanceClass()' method first.");
        }
    }

    @SuppressWarnings("unchecked")
    public T make()
    {
        try
        {
            Class<T> castClass = (Class<T>) cls;

            return ReflectionUtil.make(castClass);
        }
        catch(ClassCastException e)
        {
            throw new ClassCastException("Cannot instantiate the class, since the property class is a parent class of the instance. Consider using the 'withInstanceClass()' method first.");
        }
    }

    public <T> ReflectedField<T> field(String fieldName)
    {
        return new ReflectedField<>(this, fieldName);
    }

    public <T> ReflectedMethod<T> method(String methodName, Class<?>... parameterTypes)
    {
        return new ReflectedMethod<>(this, methodName, parameterTypes);
    }

    public ReflectedConstructor<T> constructor(Class<?>... parameterTypes)
    {
        return new ReflectedConstructor<>(this, parameterTypes);
    }

    public Class<? super T> getReflectionClass()
    {
        return cls;
    }
}
