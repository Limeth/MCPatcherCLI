package com.prupe.mcpatcher;

import com.prupe.mcpatcher.launcher.version.Version;
import cz.limeth.mcpatchercli.reflection.ReflectionUtil;

import java.io.*;

/**
 * Since there is no way to extend the {@link Version} class, I had to rewrite all the methods.
 */
public class VersionBridge
{
    public static Version getSpecific(File versionJar, File versionJson, String versionId) throws Hook.PatchException
    {
        InputStream input = null;

        if(!versionJar.isFile())
            return null;

        try
        {
            updateJson(versionJar, versionId, false);

            if(!versionJson.isFile())
                return null;

            input = new FileInputStream(versionJson);
            InputStreamReader reader = new InputStreamReader(input);
            return JsonUtils.newGson().fromJson(reader, Version.class);
        }
        catch (FileNotFoundException | PatcherException e)
        {
            throw new Hook.PatchException(e);
        }
        finally
        {
            MCPatcherUtils.close(input);
        }
    }

    @Deprecated
    public static Version getLocalVersion(File versionsDir, String id)
    {
        InputStream input = null;
        File local = getJarPath(versionsDir, id);
        if (!local.isFile()) {
            return null;
        }
        try {
            fetchJson(versionsDir, id, false);
            local = getJsonPath(versionsDir, id);
            if (!local.isFile()) {
                return null;
            }
            input = new FileInputStream(local);
            InputStreamReader reader = new InputStreamReader(input);
            return JsonUtils.newGson().fromJson(reader, Version.class);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        } finally {
            MCPatcherUtils.close(input);
        }
    }

    @Deprecated
    public static File getJarPath(File versionsDir, String id)
    {
        return new File(versionsDir, id + File.separatorChar + id + ".jar");
    }

    @Deprecated
    public static File getJsonPath(File versionsDir, String id)
    {
        return new File(versionsDir, id + File.separatorChar + id + ".json");
    }

    public static void updateJson(File versionJson, String id, boolean forceRemote) throws PatcherException
    {
        if(forceRemote || !versionJson.isFile())
        {
            Util.fetchURL(Version.getJsonURL(id), versionJson, forceRemote, Util.LONG_TIMEOUT, Util.JSON_SIGNATURE);
        }
    }

    @Deprecated
    public static void fetchJson(File versionsDir, String id, boolean forceRemote) throws PatcherException
    {
        File path = getJsonPath(versionsDir, id);
        if(forceRemote || !path.isFile())
        {
            Util.fetchURL(Version.getJsonURL(id), path, forceRemote, Util.LONG_TIMEOUT, Util.JSON_SIGNATURE);
        }
    }

    @Deprecated
    public static boolean isComplete(File versionsDir, String id)
    {
        return getJsonPath(versionsDir, id).isFile() && getJarPath(versionsDir, id).isFile();
    }
}
