package cz.limeth.mcpatchercli;

import com.beust.jcommander.*;
import com.beust.jcommander.converters.FileConverter;
import com.beust.jcommander.internal.Lists;
import com.prupe.mcpatcher.MCPatcher;
import com.prupe.mcpatcher.MCPatcherUtils;
import cz.limeth.mcpatchercli.reflection.ReflectionUtil;
import lombok.Getter;

import java.io.File;
import java.util.List;

public final class Arguments
{
    @Parameter(names = {"-h", "--help"}, help = true, description = "List command-line arguments.")
    public boolean help;

    @Parameter(names = {"-l", "--log"}, description = "How verbose should MCPatcher be?")
    public int logLevel;

    @Parameter(names = {"--listMods"}, description = "Display available mods and exit")
    public boolean listMods;

    @Parameter(names = {"-m", "--mods"}, variableArity = true, listConverter = StringListConverter.class,
            description = "Select only specific mods.")
    public List<String> selectedMods;

    @Parameter(names = {"-d", "--directory", "--minecraftDirectory"}, converter = FileConverter.class,
            validateValueWith = DirectoryValidator.class, description = "The Minecraft directory.")
    public File minecraftDirectory;

    @Parameter(names = {"-v", "--version"}, description = "Force a version of the input JAR file.")
    public String version;

    @Parameter(names = {"-i", "--jar", "--input", "--inputJar"}, converter = FileConverter.class,
            validateValueWith = ExistingFileValidator.class, description = "The input version JAR.", required = true)
    public File inputJar;

    @Parameter(names = {"-j", "--json", "--inputJson"}, converter = FileConverter.class,
            validateValueWith = ExistingFileValidator.class, description = "The input version JSON.", required = true)
    public File inputJson;

    @Parameter(names = {"-o", "--outputJar"}, converter = FileConverter.class,
            validateValueWith = NotDirectoryValidator.class, description = "The outputJar version JAR.", required = true)
    public File outputJar;

    public static class StringListConverter implements IStringConverter<List<String>>
    {
        @Override
        public List<String> convert(String value)
        {
            List<String> result = Lists.newArrayList();

            for(String mod : value.split(","))
                result.add(mod.trim());

            return result;
        }
    }

    public static class ExistingFileValidator implements IValueValidator<File>
    {
        @Override
        public void validate(String name, File value) throws ParameterException
        {
            if(!value.isFile())
                throw new ParameterException("Argument '" + name + "' is not a file.");
        }
    }

    public static class NotDirectoryValidator implements IValueValidator<File>
    {
        @Override
        public void validate(String name, File value) throws ParameterException
        {
            if(value.isDirectory())
                throw new ParameterException("Argument '" + name + "' must not be a directory.");
        }
    }

    public static class DirectoryValidator implements IValueValidator<File>
    {
        @Override
        public void validate(String name, File value) throws ParameterException
        {
            if(!value.isDirectory())
                throw new ParameterException("Argument '" + name + "' must be a directory.");
        }
    }
}
