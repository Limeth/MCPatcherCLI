package cz.limeth.mcpatchercli.reflection;

import com.beust.jcommander.internal.Lists;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.util.Arrays;

@SuppressWarnings("unused")
public class ReflectedConstructor<T>
{
    @Getter private final WrappedReflection<T> reflection;
    private final Class<?>[] parameterTypes;

    @SneakyThrows
    public ReflectedConstructor(@NonNull WrappedReflection<T> reflection, Class<?>... parameterTypes)
    {
        reflection.getReflectionClass().getDeclaredConstructor(parameterTypes);

        this.reflection = reflection;
        this.parameterTypes = Arrays.copyOf(parameterTypes, parameterTypes.length);
    }

    public T make(Object... parameters)
    {
        return reflection.make(Lists.newArrayList(parameterTypes), Lists.newArrayList(parameters));
    }

    public Class<?>[] getParameterTypes()
    {
        return Arrays.copyOf(parameterTypes, parameterTypes.length);
    }
}

