package cz.limeth.mcpatchercli.reflection;

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;

@SuppressWarnings("unused")
public class ReflectedField<T>
{
    private final WrappedReflection<?> reflection;
    @Getter private final String fieldName;

    @SneakyThrows
    public ReflectedField(@NonNull WrappedReflection<?> reflection, @NonNull String fieldName)
    {
        reflection.getReflectionClass().getDeclaredField(fieldName);

        this.reflection = reflection;
        this.fieldName = fieldName;
    }

    public T get()
    {
        return reflection.get(fieldName);
    }

    public WrappedReflection<T> wrap()
    {
        return ReflectionUtil.wrap(get());
    }

    public void set(T value)
    {
        reflection.set(fieldName, value);
    }

    @SuppressWarnings("unchecked")
    public <E> WrappedReflection<E> getReflection()
    {
        return (WrappedReflection<E>) reflection;
    }
}
