package cz.limeth.mcpatchercli.reflection;

import com.beust.jcommander.internal.Lists;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.util.Arrays;

@SuppressWarnings("unused")
public class ReflectedMethod<T>
{
    private final WrappedReflection<?> reflection;
    @Getter private final String methodName;
    private final Class<?>[] parameterTypes;

    @SneakyThrows
    public ReflectedMethod(@NonNull WrappedReflection<?> reflection, @NonNull String methodName, Class<?>... parameterTypes)
    {
        reflection.getReflectionClass().getDeclaredMethod(methodName, parameterTypes);

        this.reflection = reflection;
        this.methodName = methodName;
        this.parameterTypes = Arrays.copyOf(parameterTypes, parameterTypes.length);
    }

    public T call(Object... parameters)
    {
        return reflection.call(methodName, Lists.newArrayList(parameterTypes), Lists.newArrayList(parameters));
    }

    public Class<?>[] getParameterTypes()
    {
        return Arrays.copyOf(parameterTypes, parameterTypes.length);
    }

    @SuppressWarnings("unchecked")
    public <E> WrappedReflection<E> getReflection()
    {
        return (WrappedReflection<E>) reflection;
    }
}
