package cz.limeth.mcpatchercli.reflection;

import com.beust.jcommander.internal.Lists;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Class used to bypass Java visibility keywords.
 */
@SuppressWarnings("unused")
public final class ReflectionUtil
{
    private ReflectionUtil() {}

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <E, T> T get(@NonNull Class<? extends E> cls, E instance, @NonNull String fieldName)
    {
        Field field = cls.getDeclaredField(fieldName);
        boolean accessible = field.isAccessible();

        try
        {
            if (!accessible)
                field.setAccessible(true);

            return (T) field.get(instance);
        }
        finally
        {
            if (!accessible)
                field.setAccessible(false);
        }
    }

    public static <T> T get(@NonNull Object instance, @NonNull String fieldName)
    {
        return get(instance.getClass(), instance, fieldName);
    }

    public static <T> T get(@NonNull Class<?> cls, @NonNull String fieldName)
    {
        return get(cls, null, fieldName);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <E> void set(@NonNull Class<? extends E> cls, E instance, @NonNull String fieldName, Object value)
    {
        Field field = cls.getDeclaredField(fieldName);
        boolean accessible = field.isAccessible();

        try
        {
            if (!accessible)
                field.setAccessible(true);

            field.set(instance, value);
        }
        finally
        {
            if (!accessible)
                field.setAccessible(false);
        }
    }

    public static void set(@NonNull Object instance, @NonNull String fieldName, Object value)
    {
        set(instance.getClass(), instance, fieldName, value);
    }

    public static void set(@NonNull Class<?> cls, @NonNull String fieldName, Object value)
    {
        set(cls, null, fieldName, value);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <T, E> T call(@NonNull Class<? extends E> cls, E instance, @NonNull String methodName, List<Class<?>> parameterTypes, List<Object> parameters)
    {
        if(parameterTypes == null)
            parameterTypes = Lists.newArrayList();
        if(parameters == null)
            parameters = Lists.newArrayList();
        if(parameterTypes.size() != parameters.size())
            throw new IllegalArgumentException("The number of parameters must be the same as the number of parameter types.");

        Method method = cls.getDeclaredMethod(methodName, parameterTypes.toArray(new Class[parameterTypes.size()]));
        boolean accessible = method.isAccessible();

        try
        {
            if (!accessible)
                method.setAccessible(true);

            return (T) method.invoke(instance, parameters.toArray());
        }
        finally
        {
            if (!accessible)
                method.setAccessible(false);
        }
    }

    public static <T, E> T call(@NonNull Class<? extends E> cls, E instance, @NonNull String methodName)
    {
        return call(cls, instance, methodName, null, null);
    }

    public static <T> T call(@NonNull Object instance, @NonNull String methodName, List<Class<?>> parameterTypes, List<Object> parameters)
    {
        return call(instance.getClass(), instance, methodName, parameterTypes, parameters);
    }

    public static <T> T call(@NonNull Object instance, @NonNull String methodName)
    {
        return call(instance.getClass(), instance, methodName);
    }

    public static <T> T call(@NonNull Class<?> cls, @NonNull String methodName, List<Class<?>> parameterTypes, List<Object> parameters)
    {
        return call(cls, null, methodName, parameterTypes, parameters);
    }

    public static <T> T call(@NonNull Class<?> cls, @NonNull String methodName)
    {
        return call(cls, null, methodName);
    }

    @SneakyThrows
    public static <T> T make(@NonNull Class<T> cls, List<Class<?>> parameterTypes, List<Object> parameters)
    {
        if(parameterTypes == null)
            parameterTypes = Lists.newArrayList();
        if(parameters == null)
            parameters = Lists.newArrayList();
        if(parameterTypes.size() != parameters.size())
            throw new IllegalArgumentException("The number of parameters must be the same as the number of parameter types.");

        Constructor<T> constructor = cls.getDeclaredConstructor(parameterTypes.toArray(new Class[parameterTypes.size()]));
        boolean accessible = constructor.isAccessible();

        try
        {
            if (!accessible)
                constructor.setAccessible(true);

            return constructor.newInstance(parameters.toArray());
        }
        finally
        {
            if (!accessible)
                constructor.setAccessible(false);
        }
    }

    public static <T> T make(@NonNull Class<T> cls)
    {
        return make(cls, null, null);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows
    public static <T> Class<T> classOf(String path)
    {
        return (Class<T>) Class.forName(path);
    }

    public static <T> WrappedReflection<T> wrap(Class<? super T> cls, @NonNull T value)
    {
        return new WrappedReflection<>(cls, value);
    }

    public static <T> WrappedReflection<T> wrap(@NonNull T value)
    {
        return new WrappedReflection<>(value);
    }

    public static <T> WrappedReflection<T> wrap(@NonNull Class<T> cls)
    {
        return new WrappedReflection<>(cls);
    }
}
